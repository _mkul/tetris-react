import { TetrominoState } from '../store/shape/types';
import { TetrominoType } from '../store/shape/shapes';
import { FieldType } from '../store/board/types';
const BoardConsts = {
  Size: { width: 20, height: 20 },
  gridSize: 20,
  bkgColor: 'rgb(200,200,200)',
  fillColor: 'rgb(200,0,0)',
  primaryColor: 'rgb(225,0, 0)',
  RED: 'rgb(200, 0, 0)',
};

export const tetrominoTypeToFieldType: { [index: string]: FieldType } = {
  I: FieldType.BrickBlue,
  J: FieldType.BrickGreen,
  L: FieldType.BrickPink,
  S: FieldType.BrickRed,
  T: FieldType.BrickViolet,
  Z: FieldType.BrickYellow,
};

export default BoardConsts;

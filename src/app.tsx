import './app.scss';
import BoardHtml from './components/board-html';
import { RootState } from './store';
import { ShapeActionType, TetrominoState } from './store/shape/types';
import { TetrominoType } from './store/shape/shapes';
import { FieldType, BoardActionType } from './store/board/types';
import { connect } from 'react-redux';
import CollisionService from './services/collision.service';
import { getBoardWithShape } from './store/board/selectors';
import React, { KeyboardEvent } from 'react';

interface MainProps {
  fields: FieldType[][];
  tetromino: TetrominoState;
  moveDown: () => {};
  moveLeft: () => {};
  moveRight: () => {};
  rotate: () => {};
  settle: (fields: FieldType[][], tetromino: TetrominoState) => {};
  reset: (newShapeType: TetrominoType) => {};
}

class App extends React.Component<MainProps, { startEnabled: boolean }> {
  constructor(props: any) {
    super(props);
    this.state = { startEnabled: true };
  }

  private overlaps(yPositionOffset = 0, xPositionOffset = 0): boolean {
    return CollisionService.overlaps(
      {
        ...this.props.tetromino,
        positionX: this.props.tetromino.positionX + xPositionOffset,
        positionY: this.props.tetromino.positionY + yPositionOffset,
      },
      this.props.fields
    );
  }

  onStart(): void {
    this.setState({ startEnabled: false });
    const timerId = setInterval(() => {
      if (this.overlaps(1)) {
        this.props.settle(this.props.fields, this.props.tetromino);
        const tetrominoTypes = Object.values(TetrominoType);
        const selectedTypeIndex = Math.floor(
          Math.random() * tetrominoTypes.length
        );
        this.props.reset(tetrominoTypes[selectedTypeIndex]);
        if (this.overlaps()) {
          clearInterval(timerId);
          this.setState({ startEnabled: true });
          alert('Game over');
        }
      } else {
        this.props.moveDown();
      }
    }, 1000);
  }

  onPause(): void {
    this.setState({ startEnabled: true });
  }

  keyboardActions: { [key: string]: () => void } = {
    ' ': () => this.props.rotate(),
    ArrowLeft: () => {
      if (!this.overlaps(0, -1)) {
        this.props.moveLeft();
      }
    },
    ArrowRight: () => {
      if (!this.overlaps(0, 1)) {
        this.props.moveRight();
      }
    },
  };

  onKeyUp(event: KeyboardEvent<HTMLDivElement>): void {
    const action = this.keyboardActions[event.key];
    action && action();
  }

  render() {
    return (
      <div
        className="main-container"
        tabIndex={0}
        onKeyUp={this.onKeyUp.bind(this)}
      >
        <div className="board-container">
          <div className="button-row">
            <button
              disabled={!this.state.startEnabled}
              onClick={this.onStart.bind(this)}
            >
              Start
            </button>
            <button
              disabled={this.state.startEnabled}
              onClick={this.onPause.bind(this)}
            >
              Pause
            </button>
            <button>Restart</button>
          </div>
          <BoardHtml
            boardWithTetromino={getBoardWithShape(
              this.props.fields,
              this.props.tetromino
            )}
          ></BoardHtml>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  moveDown: () => ({ type: ShapeActionType.MoveDown }),
  moveLeft: () => ({ type: ShapeActionType.MoveLeft }),
  moveRight: () => ({ type: ShapeActionType.MoveRight }),
  rotate: () => ({ type: ShapeActionType.Rotate }),
  reset: (newShapeType: TetrominoType) => ({
    type: ShapeActionType.Reset,
    payload: { type: newShapeType },
  }),
  settle: (fields: FieldType[][], tetromino: TetrominoState) => ({
    type: BoardActionType.SettleTetromino,
    payload: { fields, tetromino },
  }),
};

const mapStateToProps = ({ board: { fields }, tetromino }: RootState) => {
  return {
    fields,
    tetromino,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

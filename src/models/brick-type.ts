export enum BrickType {
  Empty = 'Empty',
  Wall = 'Wall',
  RedBrick = 'RedBrick',
  GreenBrick = 'GreenBrick',
}

import React from 'react';
import './board-html.scss';
import { FieldType } from '../store/board/types';

interface BoardComponentProps {
  boardWithTetromino: FieldType[][];
}

const BoardFieldToCssClass: { [key: string]: string } = {
  Empty: '',
  BrickBlue: 'brick-blue',
  BrickGreen: 'brick-green',
  BrickPink: 'brick-pink',
  BrickRed: 'brick-red',
  BrickViolet: 'brick-violet',
  BrickYellow: 'brick-yellow',
};

class BoardHtml extends React.Component<BoardComponentProps> {
  render() {
    return (
      <div className="board-wrapper">
        <div className="board">
          {this.props.boardWithTetromino.map((row, y) => (
            <div key={`row-${y}`} className="board__row">
              {row.map((field, x) => (
                <div
                  key={`field-${x}`}
                  className={`board__field ${BoardFieldToCssClass[field]}`}
                ></div>
              ))}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default BoardHtml;

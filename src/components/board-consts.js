const BoardConsts = {
  Size: { width: 20, height: 20 },
  gridSize: 20,
  bkgColor: "rgb(200,200,200)",
  fillColor: "rgb(200,0,0)",
  primaryColor: "rgb(225,0, 0)",
  RED: "rgb(200, 0, 0)",
};

export default BoardConsts;

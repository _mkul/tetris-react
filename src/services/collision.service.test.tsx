import { Shape } from '../models/shape';
import CollisionService from './collision.service';

describe('collisions with board borders', () => {
    const board = [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ];

    test('should detect collision with bottom border', () => {
        const shapes: Shape[] = [
            { position: { x: 2, y: 3 }, geometry: [[1, 1], [1, 1]] },
            { position: { x: 2, y: 3 }, geometry: [[1, 1], [1, 1]] },
            { position: { x: 4, y: 0 }, geometry: [[1, 1], [1, 1]] },
            { position: { x: -1, y: 0 }, geometry: [[1, 1], [1, 1]] }
        ]

        shapes.forEach(shape => {
            const actual = CollisionService.overlaps(shape, board);

            expect(actual).toBeTruthy();
        })
    });

    test('should NOT detect collision with bottom border', () => {
        const shapes: Shape[] = [
            { position: { x: 2, y: 2 }, geometry: [[1, 1], [1, 1]] },
            { position: { x: 0, y: 0 }, geometry: [[1, 1], [1, 1]] }
        ]

        shapes.forEach(shape => {
            const actual = CollisionService.overlaps(shape, board);

            expect(actual).toBeFalsy();
        })
    });
})

describe('collisions with board items', () => {
    const board = [
        [1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 1]
    ];

    test('should detect collision', () => {
        const shapes: Shape[] = [
            { position: { x: 0, y: 0 }, geometry: [[1, 1], [1, 1]] },
            { position: { x: 1, y: 1 }, geometry: [[1, 1], [1, 1]] },
            { position: { x: 4, y: 3 }, geometry: [[1, 1], [1, 1]] }
        ]

        shapes.forEach(shape => {
            const actual = CollisionService.overlaps(shape, board);

            expect(actual).toBeTruthy();
        })
    });

    test('should NOT detect collision', () => {
        const shapes: Shape[] = [
            { position: { x: 0, y: 1 }, geometry: [[1, 1], [1, 1]] },
            { position: { x: 1, y: 0 }, geometry: [[1, 1], [1, 1]] }
        ]

        shapes.forEach(shape => {
            const actual = CollisionService.overlaps(shape, board);

            expect(actual).toBeTruthy();
        })
    });

})

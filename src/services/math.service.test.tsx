import MathService from './math.service';

test('transpose 3x3 2d array 3x3', () => {
    const source = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    ];
    const expected = [
        [7, 4, 1],
        [8, 5, 2],
        [9, 6, 3]
    ];
    const actual = MathService.transpose(source);
    expect(actual).toEqual(expected);
})

test('transpose 3x3 2d array 2x3', () => {
    const source = [
        [1, 2],
        [3, 4],
        [5, 6]
    ];
    const expected = [
        [5, 3, 1],
        [6, 4, 2]
    ];
    const actual = MathService.transpose(source);
    expect(actual).toEqual(expected);
})

test('transpose 3x3 2d array 3x2', () => {
    const source = [
        [1, 2, 3],
        [4, 5, 6]
    ];
    const expected = [
        [4, 1],
        [5, 2],
        [6, 3]
    ];
    const actual = MathService.transpose(source);
    expect(actual).toEqual(expected);
})
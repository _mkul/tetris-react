import { TetrominoState } from '../store/shape/types';
import * as fromTetrominoActions from '../store/shape/actions';
import { store } from '../store';
import CollisionService from './collision.service';
import { BoardState } from '../store/board/types';

enum GameState {
  None = 'None',
  Started = 'Started',
  Paused = 'Paused',
}

let gameState: GameState = GameState.None;

export class GameEngine {
  public static startGame(): void {
    gameState = GameState.Started;
    setInterval(() => {
      store.dispatch(fromTetrominoActions.moveDown());
    }, 1000);
  }

  handleMoveDown(tetrominoState: TetrominoState, boardState: BoardState): void {
    if (CollisionService.overlaps(tetrominoState, boardState.fields)) {
      alert('Collision');
    } else {
      fromTetrominoActions.moveDown();
    }
  }
}

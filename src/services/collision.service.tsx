import { TetrominoState } from '../store/shape/types';
import { FieldType } from '../store/board/types';

const CollisionService = {
  overlaps: (shape: TetrominoState, board: FieldType[][]): boolean => {
    for (let a = 0; a < shape.shape.length; a++) {
      for (let b = 0; b < shape.shape[a].length; b++) {
        if (!shape.shape[a][b]) {
          continue;
        }
        const boardX = b + shape.positionX;
        const boardY = a + shape.positionY;
        if (boardX < 0 || boardX > board[a].length - 1) {
          return true;
        }
        if (boardY > board.length - 1) {
          return true;
        }
        if (board[boardY][boardX] !== FieldType.Empty) {
          return true;
        }
      }
    }
    return false;
  },
};

export default CollisionService;

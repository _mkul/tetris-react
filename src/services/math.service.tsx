class MathService {
    public static transpose<T>(source: T[][]): T[][] {
        const columnCount = source[0].length;
        const rowCount = source.length;
        const result: T[][] = [];
        for (let x = 0; x < columnCount; x++) {
            const columnReversed = source.map(row => row[x]).reverse();
            result.push(columnReversed);
        }
        return result;
    }
}

export default MathService;
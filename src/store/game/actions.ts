import { GameActionTypes, GameActionType } from './types';

export function start(): GameActionTypes {
  return {
    type: GameActionType.StartGame,
  };
}

export function restart(): GameActionTypes {
  return {
    type: GameActionType.RestartGame,
  };
}

export function pause(): GameActionTypes {
  return {
    type: GameActionType.PauseGame,
  };
}

export function finish(): GameActionTypes {
  return {
    type: GameActionType.FinishGame,
  };
}

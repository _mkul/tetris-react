export enum GameActionType {
  StartGame = 'StartGame',
  RestartGame = 'RestartGame',
  PauseGame = 'PauseGame',
  FinishGame = 'FinishGame',
}

interface StartGameAction {
  type: GameActionType.StartGame;
}

interface RetartGameAction {
  type: GameActionType.RestartGame;
}

interface PauseGameAction {
  type: GameActionType.PauseGame;
}

interface FinishGameAction {
  type: GameActionType.FinishGame;
}

export type GameActionTypes =
  | StartGameAction
  | RetartGameAction
  | PauseGameAction
  | FinishGameAction;

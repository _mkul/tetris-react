import { BoardActionTypes, BoardActionType, FieldType } from './types';
import { TetrominoState } from '../shape/types';

export function settleTetromino(payload: {
  fields: FieldType[][];
  tetromino: TetrominoState;
}): BoardActionTypes {
  return {
    type: BoardActionType.SettleTetromino,
    payload,
  };
}

export function removeLastRow(): BoardActionTypes {
  return {
    type: BoardActionType.RemoveLastRow,
  };
}

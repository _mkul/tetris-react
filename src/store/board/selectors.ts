import { FieldType } from './types';
import { RootState } from '..';
import { TetrominoState } from '../shape/types';
import { tetrominoTypeToFieldType } from '../../shared/consts';

export const getBoard = (state: RootState) => state.board.fields;

export const getBoardWithShape = (
  fields: FieldType[][],
  tetromino: TetrominoState
): FieldType[][] => {
  const board: FieldType[][] = fields.map((row) => [...row]);

  const fieldType = tetrominoTypeToFieldType[tetromino.type.toString()];
  tetromino.shape.forEach((row: number[], y: number) =>
    row.forEach((field, x) => {
      if (field !== 0) {
        board[y + tetromino.positionY][x + tetromino.positionX] = fieldType;
      }
    })
  );

  return board;
};

import {
  BoardState,
  BoardActionType,
  BoardActionTypes,
  FieldType,
} from './types';
import BoardConsts from '../../shared/consts';
import { getBoardWithShape } from './selectors';

const initialState: BoardState = {
  fields: Array(BoardConsts.Size.height)
    .fill(0)
    .map((row) => Array(BoardConsts.Size.width).fill(FieldType.Empty)),
};

export function boardReducer(
  state = initialState,
  action: BoardActionTypes
): BoardState {
  switch (action.type) {
    case BoardActionType.SettleTetromino:
      return {
        ...state,
        fields: getBoardWithShape(
          action.payload.fields,
          action.payload.tetromino
        ),
      };
    case BoardActionType.RemoveLastRow:
      return {
        ...state,
        fields: state.fields.splice(state.fields.length - 1),
      };
    default:
      return state;
  }
}

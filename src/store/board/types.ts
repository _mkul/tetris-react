import { TetrominoState } from '../shape/types';
export enum FieldType {
  Empty = 'Empty',
  BrickRed = 'BrickRed',
  BrickGreen = 'BrickGreen',
  BrickBlue = 'BrickBlue',
  BrickYellow = 'BrickYellow',
  BrickPink = 'BrickPink',
  BrickViolet = 'BrickViolet',
}

export interface BoardState {
  fields: FieldType[][];
}

export enum BoardActionType {
  SettleTetromino = 'SettleTetromino',
  RemoveLastRow = 'RemoveLastRow',
}

interface SettleTetrominoAction {
  type: BoardActionType.SettleTetromino;
  payload: { fields: FieldType[][]; tetromino: TetrominoState };
}

interface RemoveLastRowAction {
  type: BoardActionType.RemoveLastRow;
}

export type BoardActionTypes = SettleTetrominoAction | RemoveLastRowAction;

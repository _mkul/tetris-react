import { combineReducers } from 'redux';
import { shapeReducer } from './shape/reducers';
import { boardReducer } from './board/reducers';
import { createStore } from 'redux';

export const rootReducer = combineReducers({
  board: boardReducer,
  tetromino: shapeReducer,
});

export const store = createStore(rootReducer);

export type RootState = ReturnType<typeof rootReducer>;

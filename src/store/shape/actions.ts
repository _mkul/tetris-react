import { ShapeActionType, ShapeActionTypes } from './types';
import { TetrominoType } from './shapes';

export function moveLeft(): ShapeActionTypes {
  return {
    type: ShapeActionType.MoveLeft,
  };
}

export function moveRight(): ShapeActionTypes {
  return {
    type: ShapeActionType.MoveRight,
  };
}

export function moveDown(): ShapeActionTypes {
  return {
    type: ShapeActionType.MoveDown,
  };
}

export function rotate(): ShapeActionTypes {
  return {
    type: ShapeActionType.Rotate,
  };
}

export function drop(): ShapeActionTypes {
  return {
    type: ShapeActionType.Drop,
  };
}

export function reset(shapeType: TetrominoType): ShapeActionTypes {
  return {
    type: ShapeActionType.Reset,
    payload: {
      type: shapeType,
    },
  };
}

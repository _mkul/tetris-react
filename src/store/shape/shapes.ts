enum TetrominoType {
  I = 'I',
  J = 'J',
  L = 'L',
  S = 'S',
  T = 'T',
  Z = 'Z',
}

const Tetrominos: { [key: string]: number[][] } = {
  I: [
    [1, 0, 0],
    [1, 0, 0],
    [1, 0, 0],
  ],
  J: [
    [0, 1, 0],
    [0, 1, 0],
    [1, 1, 0],
  ],
  L: [
    [0, 1, 0],
    [0, 1, 0],
    [0, 1, 1],
  ],
  S: [
    [0, 1, 1],
    [1, 1, 0],
    [0, 0, 0],
  ],
  T: [
    [1, 1, 1],
    [0, 1, 0],
    [0, 0, 0],
  ],
  Z: [
    [1, 1, 0],
    [0, 1, 1],
    [0, 0, 0],
  ],
};

export { Tetrominos, TetrominoType };

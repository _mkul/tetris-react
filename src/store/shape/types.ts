import { TetrominoType } from './shapes';

export interface TetrominoState {
  type: TetrominoType;
  positionX: number;
  positionY: number;
  shape: number[][];
}

export enum ShapeActionType {
  MoveLeft = 'MoveLeft',
  MoveRight = 'MoveRight',
  MoveDown = 'MoveDown',
  Rotate = 'Rotate',
  Drop = 'Drop',
  Settle = 'Settle',
  Reset = 'Reset',
}

interface MoveLeftAction {
  type: ShapeActionType.MoveLeft;
}

interface MoveRightAction {
  type: ShapeActionType.MoveRight;
}

interface MoveDownAction {
  type: ShapeActionType.MoveDown;
}

interface RotateAction {
  type: ShapeActionType.Rotate;
}

interface DropAction {
  type: ShapeActionType.Drop;
}

interface ResetAction {
  type: ShapeActionType.Reset;
  payload: {
    type: TetrominoType;
  };
}

export type ShapeActionTypes =
  | MoveLeftAction
  | MoveRightAction
  | MoveDownAction
  | RotateAction
  | DropAction
  | ResetAction;

import { TetrominoState, ShapeActionTypes, ShapeActionType } from './types';
import { TetrominoType, Tetrominos } from './shapes';
import MathService from '../../services/math.service';

const initialState: TetrominoState = {
  type: TetrominoType.S,
  positionX: 5,
  positionY: 0,
  shape: Tetrominos.S,
};

export function shapeReducer(
  state = initialState,
  action: ShapeActionTypes
): TetrominoState {
  switch (action.type) {
    case ShapeActionType.MoveLeft:
      return {
        ...state,
        positionX: state.positionX - 1,
      };
    case ShapeActionType.MoveRight:
      return {
        ...state,
        positionX: state.positionX + 1,
      };
    case ShapeActionType.MoveDown:
      return {
        ...state,
        positionY: state.positionY + 1,
      };
    case ShapeActionType.Rotate:
      return {
        ...state,
        shape: MathService.transpose(state.shape),
      };
    case ShapeActionType.Reset:
      return {
        ...initialState,
        type: action.payload.type,
        shape: Tetrominos[action.payload.type],
      };
    default:
      return state;
  }
}
